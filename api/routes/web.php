<?php

Route::get('/home', function () {
    return redirect()->to('ticket');
})->name('home');

Route::get('/', function () {
    return redirect()->to('ticket');
});

Auth::routes();

Route::resource('user', 'UserController', ['except' => ['show']]);
Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);

Route::resource('ticket', 'TicketController', ['except' => ['destroy']]);
Route::get('ticket/{ticket}/attachment', 'TicketController@downloadAttachment')->name('ticket.attachment.get');
Route::post('ticket/{ticket}/delete', 'TicketController@destroy')->name('ticket.destroy');
Route::post('ticket/{ticket}/accept', 'TicketWorkflowController@accept')->name('ticket.accept');
Route::post('ticket/{ticket}/reject', 'TicketWorkflowController@reject')->name('ticket.reject');
Route::get('login/custom', 'Auth\LoginController@login');